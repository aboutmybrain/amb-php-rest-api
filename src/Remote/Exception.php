<?php


namespace AMB\Remote;


use AMB\Remote\Helper\ToArrayTrait;
use Yiisoft\FriendlyException\FriendlyExceptionInterface;

class Exception extends \Exception implements FriendlyExceptionInterface
{
    use ToArrayTrait;

    /**
     * @var string
     */
    protected $message = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $solution = '';

    public function __construct(string $message = "")
    {
        parent::__construct($message, 0, null);
        $this->_clear_unused_properties();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSolution(): string
    {
        return $this->solution;
    }

    public function setSolution(string $solution): void
    {
        $this->solution = $solution;
    }

    public function __toString()
    {
        $this->_clear_unused_properties();
        return json_encode($this->_toArray());
    }

    private function _clear_unused_properties()
    {
        $this->code = 0;
        $this->file = '';
        $this->line = 0;
    }
}
