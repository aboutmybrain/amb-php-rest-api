<?php

namespace AMB\Remote;

use AMB\Remote\Api\BaseSegment;
use AMB\Remote\Api\Order;
use AMB\Remote\Api\Products;
use AMB\Remote\Api\Verify;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Cookie\CookieJar;

/**
 * @method Products Products()
 * @method Order Order()
 * @method Verify Verify()
 */
class ApiApplication
{
    protected string $baseUrl = 'https://login.aboutmybrain.com/api';
    private array $config;
    private ClientInterface $client;
    private array $options = [
        'headers' => [],
        'body' => '',
        'cookies' => false
    ];

    public function __construct($config = [])
    {
        $this->config = $config;
        $this->client = new Client();

        $this->buildAuthHeader();
    }

    public function buildAuthHeader(): void
    {
        if (!isset($this->config['client_id'])) {
            return;
        }

        if (!isset($this->config['client_secret'])) {
            return;
        }

        $credential = base64_encode("{$this->config['client_id']}:{$this->config['client_secret']}");
        $this->options['headers']['Authorization'] = "Basic {$credential}";

        $cookieJar = CookieJar::fromArray([
            'XDEBUG_SESSION' => 'douglas'
        ], 'oc.aboutmybrain.com');

        $this->options['cookies'] = $cookieJar;
    }

    public function __call($name, $arguments)
    {
        $segment_class = "AMB\\Remote\\Api\\{$name}";
        if (class_exists($segment_class)) {
            /** @var BaseSegment $segment */
            $segment = new $segment_class(
                $this->client,
                $this->baseUrl,
                $this->options
            );
            return $segment;
        }

        return null;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }
}
