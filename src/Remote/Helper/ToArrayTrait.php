<?php


namespace AMB\Remote\Helper;


use ArrayAccess;
use ReflectionClass;
use ReflectionException;

trait ToArrayTrait
{
    /**
     * Obtain the array of the current class regardless of member visibility
     * @return array
     */
    private function _toArray(): array
    {
        $obj2arr = function ($var) use (&$obj2arr) {
            try {
                if (is_array($var) || $var instanceof ArrayAccess) {
                    $array = [];
                    foreach ($var as $item) {
                        $array[] = $obj2arr($item);
                    }
                    return $array;
                }

                if (is_object($var)) {
                    $array = [];
                    $reflectionClass = new ReflectionClass(get_class($var));
                    foreach ($reflectionClass->getProperties() as $property) {
                        $property->setAccessible(true);

                        $array[$property->getName()] = $obj2arr($property->getValue($var));

                        $property->setAccessible(false);
                    }
                    return $array;
                }
            } catch (ReflectionException $e) {
            }

            return $var;
        };

        return $obj2arr($this);
    }
}
