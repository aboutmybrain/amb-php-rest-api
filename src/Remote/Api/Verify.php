<?php


namespace AMB\Remote\Api;


use AMB\Remote\Api\Verify\Priv;
use AMB\Remote\Api\Verify\Pub;

class Verify extends BaseSegment
{
    public function Priv(): Priv
    {
        return (new Priv($this->client, $this->getBaseUrl(), $this->options));
    }

    public function Pub(): Pub
    {
        return (new Pub($this->client, $this->getBaseUrl(), $this->options));
    }
}