<?php


namespace AMB\Remote\Api;


use GuzzleHttp\Exception\GuzzleException;
use JsonMapper;
use JsonMapper_Exception;

class Products extends BaseSegment
{
    /**
     * @return \AMB\Remote\Model\Products
     * @throws GuzzleException
     * @throws JsonMapper_Exception
     */
    public function get(): \AMB\Remote\Model\Products
    {
        $response = $this->client->request(
            'get',
            $this->getUrl('/products')
        );

        $mapper = new JsonMapper();

        $json = json_decode($response->getBody()->getContents());

        /** @var \AMB\Remote\Model\Products $products */
        $products = $mapper->map($json,
            new \AMB\Remote\Model\Products());

        return $products;
    }
}
