<?php


namespace AMB\Remote\Api\Order;


use AMB\Remote\Api\BaseSegment;
use AMB\Remote\Model\HyperLink;
use GuzzleHttp\Exception\GuzzleException;
use JsonMapper;
use JsonMapper_Exception;

class ClaimPage extends BaseSegment
{
    /**
     * @param string $source_id
     * @param string $order_id
     * @return HyperLink
     * @throws GuzzleException
     * @throws JsonMapper_Exception
     */
    public function get(string $source_id, string $order_id): HyperLink
    {
        $response = $this->client->request(
            'get',
            $this->getUrl("/order/{$source_id}/{$order_id}/claim-page")
        );

        $mapper = new JsonMapper();

        $json = json_decode($response->getBody()->getContents());

        /** @var HyperLink $output */
        $output = $mapper->map($json, new HyperLink());

        return $output;
    }
}
