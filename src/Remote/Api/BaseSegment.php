<?php


namespace AMB\Remote\Api;


use GuzzleHttp\ClientInterface;

abstract class BaseSegment
{
    protected ClientInterface $client;

    protected array $options;

    protected string $baseUrl = 'https://login.aboutmybrain.com/api';

    public function __construct(
        ClientInterface $client,
        string $baseUrl,
        array $options = []
    ) {
        $this->client = $client;
        $this->baseUrl = $baseUrl;
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     * @return $this
     */
    public function setBaseUrl(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @param string $endpoint
     * @return string
     */
    protected function getUrl(string $endpoint): string
    {
        return "{$this->baseUrl}{$endpoint}";
    }
}