<?php


namespace AMB\Remote\Api;


use AMB\Remote\Api\Order\ClaimPage;
use AMB\Remote\Model\OrderResponse;
use GuzzleHttp\Exception\GuzzleException;
use JsonMapper;
use JsonMapper_Exception;

class Order extends BaseSegment
{
    /**
     * @param \AMB\Remote\Model\Order $order
     * @return OrderResponse
     * @throws GuzzleException
     * @throws JsonMapper_Exception
     */
    public function post(\AMB\Remote\Model\Order $order): OrderResponse
    {
        $this->options['body'] = $order;

        $response = $this->client->request(
            'post',
            $this->getUrl('/order'),
            $this->options
        );

        $mapper = new JsonMapper();

        $json = json_decode($response->getBody()->getContents());

        /** @var OrderResponse $output */
        $output = $mapper->map($json, new OrderResponse());

        return $output;
    }

    public function ClaimPage(): ClaimPage
    {
        return (new ClaimPage($this->client, $this->getBaseUrl(),
            $this->options));
    }
}
