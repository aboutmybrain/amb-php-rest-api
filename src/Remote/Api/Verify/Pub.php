<?php


namespace AMB\Remote\Api\Verify;


use AMB\Remote\Api\BaseSegment;
use GuzzleHttp\Exception\GuzzleException;
use JsonMapper;
use JsonMapper_Exception;

class Pub extends BaseSegment
{
    /**
     * @return \AMB\Remote\Model\Pub
     * @throws GuzzleException
     * @throws JsonMapper_Exception
     */
    public function get(): \AMB\Remote\Model\Pub
    {
        $response = $this->client->request(
            'get',
            $this->getUrl('/verify/pub')
        );

        $mapper = new JsonMapper();

        $json = json_decode($response->getBody()->getContents());

        /** @var \AMB\Remote\Model\Pub $output */
        $output = $mapper->map($json, new \AMB\Remote\Model\Pub());

        return $output;
    }
}