<?php


namespace AMB\Remote\Api\Verify;


use AMB\Remote\Api\BaseSegment;
use GuzzleHttp\Exception\GuzzleException;
use JsonMapper;
use JsonMapper_Exception;

class Priv extends BaseSegment
{
    /**
     * @return \AMB\Remote\Model\Priv
     * @throws GuzzleException
     * @throws JsonMapper_Exception
     */
    public function get(): \AMB\Remote\Model\Priv
    {
        $response = $this->client->request(
            'get',
            $this->getUrl('/verify/priv')
        );

        $mapper = new JsonMapper();

        $json = json_decode($response->getBody()->getContents());

        /** @var \AMB\Remote\Model\Priv $output */
        $output = $mapper->map($json, new \AMB\Remote\Model\Priv());

        return $output;
    }
}