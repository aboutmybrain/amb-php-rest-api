<?php


namespace AMB\Remote\Model;


class HyperLink extends BaseModel
{
    protected string $text = '';

    protected string $href = '';

    public function getHref(): string
    {
        return $this->href;
    }

    public function setHref(string $href): void
    {
        $this->href = $href;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}
