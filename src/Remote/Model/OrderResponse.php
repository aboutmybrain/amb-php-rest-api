<?php


namespace AMB\Remote\Model;


class OrderResponse extends BaseModel
{
    protected string $id = '';

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }
}