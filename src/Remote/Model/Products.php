<?php


namespace AMB\Remote\Model;


class Products extends BaseModel
{
    /**
     * @var Product[]
     */
    protected array $items = [];

    /**
     * @return Product[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Product[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }
}