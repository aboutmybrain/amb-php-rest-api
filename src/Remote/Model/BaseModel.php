<?php

namespace AMB\Remote\Model;

use AMB\Remote\Helper\ToArrayTrait;

abstract class BaseModel implements iModel
{
    use ToArrayTrait;

    public function __toString(): string
    {
        return json_encode($this->toArray());
    }

    public function toArray(): array
    {
        return $this->_toArray();
    }
}
