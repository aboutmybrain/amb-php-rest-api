<?php

namespace AMB\Remote\Model;

class Product extends BaseModel
{
    protected string $id = '';

    protected string $name = '';

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
}