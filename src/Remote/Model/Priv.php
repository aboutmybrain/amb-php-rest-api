<?php

namespace AMB\Remote\Model;

class Priv extends BaseModel
{
    protected string $text = '';

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}