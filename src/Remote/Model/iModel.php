<?php


namespace AMB\Remote\Model;


interface iModel
{
    public function __toString(): string;

    public function toArray(): array;
}
