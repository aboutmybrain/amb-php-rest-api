<?php


namespace AMB\Remote\Model;


class Customer extends BaseModel
{
    protected string $email = '';

    protected bool $is_practitioner = false;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function getIsPractitioner(): bool
    {
        return $this->is_practitioner;
    }

    /**
     * @param bool $is_practitioner
     */
    public function setIsPractitioner(bool $is_practitioner): void
    {
        $this->is_practitioner = $is_practitioner;
    }
}