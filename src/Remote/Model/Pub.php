<?php

namespace AMB\Remote\Model;

class Pub extends BaseModel
{
    protected string $text = '';

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}