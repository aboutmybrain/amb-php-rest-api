<?php

namespace AMB\Remote\Model;

class LineItem extends BaseModel
{
    protected string $product_id = '';

    protected int $quantity = 1;

    public function getProductId(): string
    {
        return $this->product_id;
    }

    public function setProductId(string $product_id): void
    {
        $this->product_id = $product_id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }
}