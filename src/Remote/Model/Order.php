<?php

namespace AMB\Remote\Model;

class Order extends BaseModel
{
    protected string $id = '';

    protected string $source_id = '';

    /**
     * @var LineItem[]
     */
    protected array $line_items = [];

    protected string $email = '';

    protected Customer $customer;

    public function __construct()
    {
        $this->customer = new Customer();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getSourceId(): string
    {
        return $this->source_id;
    }

    public function setSourceId(string $source_id): void
    {
        $this->source_id = $source_id;
    }

    /**
     * @return LineItem[]
     */
    public function getLineItems(): array
    {
        return $this->line_items;
    }

    /**
     * @param LineItem[] $line_items
     */
    public function setLineItems(array $line_items): void
    {
        $this->line_items = $line_items;
    }

    public function addLineItem(LineItem $lineItem): void
    {
        $this->line_items[] = $lineItem;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }
}