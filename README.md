# PHP API library for About my Brain system

## Usage
```
    require 'vendor/autoload.php';

    $api = new \AMB\Remote\ApiApplication([
        'client_id' => '[client id]',
        'client_secret' => '[client secret]',
    ]);

    $api->Verify()->Pub()->get();
```
